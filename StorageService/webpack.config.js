const { resolve } = require('path');
const { dependencies } = require('./app/package.json');
console.log(dependencies);
const nodeModules = {};

Object
.keys(dependencies)
.forEach((mod) => {
 nodeModules[mod] = `commonjs ${mod}`;
});


module.exports = (env = { prod: true }) => ({
    context: resolve(__dirname, './'),
    entry: {
     server: env.prod ? './app/index.js' : ['webpack/hot/poll?1000', './app/index.js']
    },
    target: 'node',
    output: {
     filename: 'server.js',
     path: resolve(__dirname, './build'),
     pathinfo: !env.prod
    },
    devtool: env.prod ? 'source-map' : 'eval'
});
