class APIError {
  constructor(result, message, errors) {
    this.result = result
    this.message = message
    this.errors  = errors
  }
} 

class SubError {
  constructor(fields = [], messages = []) {
    this.errors = [];
    fields.forEach((itm, idx) => {
      this.errors.push({
        'fields': itm,
        'message': messages[idx]
      })
    });
  }
}

exports.APIError = APIError;
exports.SubError = SubError;