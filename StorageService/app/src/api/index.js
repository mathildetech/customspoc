'use strict';

const index = require( './root.controller');
const pre = require( './pre.controller');
const post  = require( './post.controller');
const register =  require( './register.controller');
const unregister =  require( './unregister.controller');
const router  = require( 'koa-router');

const root = router();
root.get('/', index.default);
root.post('/pre_validate', pre.default);
root.post('/post_validate', post.default);
root.post('/register', register.default);
root.post('/unregister', unregister.default);
module.exports = root;