'use strict';

const list = require( './root.model');

exports.default = async function index(ctx, next) {
  let data = await list();
  ctx.status = 200;
  ctx.body = data;
  await next();
}
