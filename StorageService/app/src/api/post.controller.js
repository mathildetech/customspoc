'use strict';
const { APIError, SubError} = require( './error');
const getRandomArbitrary = require('./util');
exports.default = async function post(ctx, next) {
  let body = ctx.body;
  let result = getRandomArbitrary(1, 10) < 9;
  if (!body.orderId || !result) {
    ctx.status = 400;
    let message; 
    let field; 
    if (!result) {
      message = 'lack of stock';
      field= 'unknown';
    } else {
      message = 'orderId is required';
      field= 'orderId';
    }
    console.log(message);
    let subError = new SubError([field], [message]);
    let error = new APIError(
      false,
      message, 
      subError.errors
    );  
    ctx.body = error;
  } else {
    ctx.status = 200;
    ctx.body = {'result': true};
  }
  await next();
}