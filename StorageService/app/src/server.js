'use strict';

const config = require('./config/index');
const configRoutes = require('./config/routes');
const Koa = require('koa');
const configKoa = require('./config/koa');

const app = new Koa();
app.port = config.port;

configKoa.default(app);
configRoutes.default(app);

module.exports = app;
