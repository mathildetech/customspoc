'use strict';

const path = require('path');

const config = {
  env: process.env.NODE_ENV || 'dev',
  root: path.normalize(__dirname + '../../'),
  ip: process.env.IP || '0.0.0.0',
  port: process.env.PORT || 9000,
  logType: process.env.LOGTYPE || 'dev'
};

module.exports = config;
