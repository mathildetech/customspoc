'use strict';

const mount = require('koa-mount');
const root = require('../api');

exports.default = function configRoutes(app) {
  app.use(mount('/', root.routes()));
  app.use(mount('/storage', root.routes()));

  // List Endpoints Here
}
