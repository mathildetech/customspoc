'use strict';

const config = require( './index');
const morgan = require( 'koa-morgan');
const parser = require( 'koa-bodyparser');
const compress = require( 'koa-compress');

exports.default = function configKoa(app) {
  app.use(compress());
  app.use(parser({
    strict: false
  }));

  app.use((ctx, next) => {
    ctx.body = ctx.request.body;
    return next();
  });

  app.on('error', err => console.error(err));

  app.use(morgan(config.logType));
}
