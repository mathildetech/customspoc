# Customs POC Project #

This project is for China Customs POC. This porject includes serveral micro services in Java(spring-boot), NodeJs and Frontend UI. 

### Microservices & MonitorUI ###

* EurekaServer
* ConfigServer
* Zuul(API Gateway)
* MonitorCollector
* MonitorService
* StorageService
* FeedbackService
* RealeaseService
* MonitorUI


### General Maven CLI ###
`mvn clean`
`mvn -Dmaven.test.skip=true clean spring-boot:run`
`mvn -Dmaven.test.skip=true clean install`

### CLI ###
* Zookeeper & Kafka

* EurekaServer
`docker run -d --name eureka -p 8761:8761 --network kafka-net alauda/eureka-server`

* ConfigServer
`docker run --rm --name config -p 8888:8888 --network kafka-net alauda/config-server`

* Zuul(API Gateway)

* MonitorCollector

* MonitorService

* StorageService

* FeedbackService

* RealeaseService
