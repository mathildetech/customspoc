import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import dynamic from 'dva/dynamic';

function RouterConfig({ history, app }) {
  const Figure = dynamic({
    app,
    component: () => import('./routes/Figure'),
  });

  const Page1 = dynamic({
    app,
    component: () => import('./routes/OrderFigure'),
  });

  const Page2 = dynamic({
    app,
    component: () => import('./routes/OrderList'),
  });

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Figure} />
        <Route exact path="/page1" component={Page1} />
        <Route exact path="/page2" component={Page2} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
