const dataOrigin = {
  nodes: [
    {
      id: 's1',
      value: '申报平台',
    },
    {
      id: 's2',
      value: '申报接口',
    },
    {
      id: 's1',
      value: 'S1',
    },
    {
      id: 's2',
      value: 'S2',
    },
    {
      id: 's3',
      value: 'S3',
    },
    {
      id: 's3',
      value: '任务分发服务',
    },
    {
      id: 's4',
      value: '审单服务',
    },
    {
      id: 's5',
      value: '许可证',
    },
    {
      id: 's6',
      value: '风险',
    },
    {
      id: 's7',
      value: '放行服务',
    },
    {
      id: 's8',
      value: '反馈',
    },
    {
      id: 's9',
      value: '计税',
    },
    {
      id: 'sd',
      value: '减免税',
    },
    {
      id: 'sa',
      value: '仓单',
    },
    {
      id: 'sb',
      value: '放行检查',
    },
    {
      id: 'sc',
      value: '人工审单平台',
    },
  ],
  edges: [
    {
      source: 's1',
      id: 'n1tos1',
      target: 's1',
    },
    {
      source: 's1',
      id: 'n1tos2',
      target: 's2',
    },
    {
      source: 's1',
      id: 'n1tos3',
      target: 's3',
    },
    {
      source: 's2',
      id: 'n2tos1',
      target: 's1',
    },
    {
      source: 's2',
      id: 'n2tos2',
      target: 's2',
    },
    {
      source: 's2',
      id: 'n2tos3',
      target: 's3',
    },
    {
      source: 's1',
      id: 's1tonode3',
      target: 's3',
    },
    {
      source: 's2',
      id: 's2tonode3',
      target: 's3',
    },
    {
      source: 's3',
      id: 's3tonode3',
      target: 's3',
    },
    {
      source: 's3',
      id: 'edge3',
      target: 's4',
    },
    {
      source: 's4',
      id: 'edge4',
      target: 's5',
    },
    {
      source: 's4',
      id: 'edge5',
      target: 's6',
    },
    {
      source: 's4',
      id: 'edge6',
      target: 's7',
    },
    {
      source: 's4',
      id: 'edge7',
      target: 's8',
    },
    {
      source: 's4',
      id: 'edge8',
      target: 's9',
    },
    {
      source: 's7',
      id: 'edge9',
      target: 'sa',
    },
    {
      source: 's7',
      id: 'edge10',
      target: 'sb',
    },
    {
      source: 's4',
      id: 'edge11',
      target: 'sc',
    },
    {
      source: 's9',
      id: 'edge12',
      target: 'sd',
    },
    {
      source: 'sc',
      id: '12to8',
      target: 's8',
    },
    {
      source: 'sc',
      id: '12to10',
      target: 'sa',
    },
    {
      source: 'sc',
      id: '12to11',
      target: 'sb',
    },
    {
      source: 's7',
      id: '7to5',
      target: 's5',
    },
    {
      source: 's7',
      id: '7to6',
      target: 's6',
    },
    {
      source: 's7',
      id: '7to8',
      target: 's8',
    },
    {
      source: 's7',
      id: '7to9',
      target: 's9',
    },
  ],
};

const customQuota = {
  getAnchorPoints: () => {
    return [
      [1, 0.5], // 右边的中点
      [0, 0.5], // 左边的中点
    ];
  },
  draw: (cfg, group) => {
    // 大框
    return group.addShape('rect', {
      attrs: {
        x: cfg.x - (cfg.size * 0.25),
        y: cfg.y - (cfg.size * 1.5),
        width: cfg.size,
        height: cfg.size * 3,
        fill: 'rgba(255,255,255,1)',
        stroke: 'rgba(52,142,226,1)',
        radius: 3,
      },
    });
  },
  afterDraw: (cfg, group) => {
    // 小框1
    group.addShape('rect', {
      attrs: {
        x: cfg.x,
        y: cfg.y - (cfg.size * 1.2),
        width: cfg.size / 2,
        height: (cfg.size / 7) * 3,
        fill: 'rgba(255,255,255,1)',
        stroke: 'rgba(52,142,226,1)',
        radius: 3,
      },
    });
    // 小框2
    group.addShape('rect', {
      attrs: {
        x: cfg.x,
        y: cfg.y - (cfg.size * 0.2),
        width: cfg.size / 2,
        height: (cfg.size / 7) * 3,
        fill: 'rgba(255,255,255,1)',
        stroke: 'rgba(52,142,226,1)',
        radius: 3,
      },
    });
    // 小框3
    group.addShape('rect', {
      attrs: {
        x: cfg.x,
        y: cfg.y + (cfg.size * 0.8),
        width: cfg.size / 2,
        height: (cfg.size / 7) * 3,
        fill: 'rgba(255,255,255,1)',
        stroke: 'rgba(52,142,226,1)',
        radius: 3,
      },
    });
    // 文字1
    group.addShape('text', {
      attrs: {
        text: 'S1',
        fill: 'rgba(52,142,226,1)',
        textAlign: 'center',
        textBaseline: 'middle',
        fontSize: 15,
        x: cfg.x + (cfg.size * 0.25),
        y: cfg.y - (cfg.size * 1),
      },
    });
    // 文字2
    group.addShape('text', {
      attrs: {
        text: 'S2',
        fill: 'rgba(52,142,226,1)',
        textAlign: 'center',
        textBaseline: 'middle',
        fontSize: 15,
        x: cfg.x + (cfg.size * 0.25),
        y: cfg.y,
      },
    });
    // 文字3
    group.addShape('text', {
      attrs: {
        text: 'S3',
        fill: 'rgba(52,142,226,1)',
        textAlign: 'center',
        textBaseline: 'middle',
        fontSize: 15,
        x: cfg.x + (cfg.size * 0.25),
        y: cfg.y + (cfg.size * 1),
      },
    });
  },
};

const customnodeS = {
  getAnchorPoints: () => {
    return [
      [1, 0.5], // 右边的中点
      [0, 0.5], // 左边的中点
    ];
  },
  draw: (cfg, group) => {
    return group.addShape('rect', {
      attrs: {
        x: cfg.x - (cfg.size * 0.25),
        y: cfg.y - (cfg.size * 0.15),
        width: cfg.size / 2,
        height: cfg.size * 0.3,
        fill: 'rgba(255,255,255,1)',
        stroke: 'rgba(56,87,35,1)',
        radius: 3,
      },
    });
  },
  afterDraw: (cfg, group) => {
    const origin = cfg.origin;
    const labelAttrs = {
      text: origin.value,
      fill: 'rgba(56,87,35,1)',
      textAlign: 'center',
      textBaseline: 'middle',
      fontSize: 15,
      x: cfg.x,
      y: cfg.y,
    };
    return group.addShape('text', {
      attrs: labelAttrs,
    });
  },
};

const customnode1 = {
  getAnchorPoints: () => {
    return [
      [1, 0.5], // 右边的中点
      [0, 0.5], // 左边的中点
    ];
  },
  draw: (cfg, group) => {
    const origin = cfg.origin;
    let showValueArr = [];
    let showColor = '';
    if (origin.showValue) {
      showValueArr = origin.showValue.split(' / ');
    }
    if (showValueArr.length > 0) {
      if (showValueArr[0] === '0') {
        showColor = 'rgb(252,13,27)';
      } else if (showValueArr[0] === showValueArr[1]) {
        showColor = 'rgb(26,175,84)';
      } else {
        showColor = 'rgb(253,191,45)';
      }
    } else {
      showColor = 'rgb(26,175,84)';
    }
    // 外框
    return group.addShape('rect', {
      attrs: {
        x: cfg.x - (cfg.size * 0.5),
        y: cfg.y - (cfg.size * 0.2),
        width: cfg.size * 1,
        height: cfg.size * 0.4,
        // fill: 'rgba(255,255,255,1)',
        // stroke: 'rgba(52,142,226,1)',
        fill: showColor,
        stroke: showColor,
        radius: 3,
      },
    });
  },
  afterDraw: (cfg, group) => {
    const origin = cfg.origin;
    // 内框上
    group.addShape('rect', {
      attrs: {
        x: cfg.x + (cfg.size * 0.1),
        y: cfg.y - (cfg.size * 0.35),
        width: cfg.size * 0.6,
        height: cfg.size * (0.4 / 2),
        fill: 'rgba(74,36,106,1)',
        stroke: 'rgba(74,36,106,1)', // 紫色
        radius: 3,
      },
    });
    // 内框下
    // group.addShape('rect', {
    //   attrs: {
    //     x: cfg.x + (cfg.size * 0.1),
    //     y: cfg.y + (cfg.size * 0.15),
    //     width: cfg.size * 0.6,
    //     height: cfg.size * (0.4 / 2),
    //     fill: 'rgba(255,255,255,1)',
    //     stroke: 'rgba(52,142,226,1)',
    //     radius: 3,
    //   },
    // });
    // 文字上
    group.addShape('text', {
      attrs: {
        text: origin.showValue,
        fill: 'rgba(255,255,255,1)',
        textAlign: 'center',
        textBaseline: 'middle',
        fontSize: 12,
        x: cfg.x + (cfg.size * 0.4),
        y: cfg.y - (cfg.size * 0.25),
      },
    });
    // 文字下
    // group.addShape('text', {
    //   attrs: {
    //     text: '999/1k',
    //     fill: 'rgba(52,142,226,1)',
    //     textAlign: 'center',
    //     textBaseline: 'middle',
    //     fontSize: 12,
    //     x: cfg.x + (cfg.size * 0.4),
    //     y: cfg.y + (cfg.size * 0.25),
    //   },
    // });
    // 服务名称文字
    const labelAttrs = {
      text: origin.value,
      fill: 'rgba(255,255,255,1)',
      textAlign: 'center',
      textBaseline: 'middle',
      fontSize: 15,
      x: cfg.x + (cfg.size * 0),
      y: cfg.y,
    };
    return group.addShape('text', {
      attrs: labelAttrs,
    });
  },
};

const customnode2 = {
  getAnchorPoints: () => {
    return [
      [1, 0.5], // 右边的中点
      [0, 0.5], // 左边的中点
    ];
  },
  draw: (cfg, group) => {
    const origin = cfg.origin;
    if (origin.nodeColor === undefined) {
      origin.nodeColor = 'rgb(26,175,84)';
    }
    // 外框
    return group.addShape('rect', {
      attrs: {
        x: cfg.x - (cfg.size * 0.5),
        y: cfg.y - (cfg.size * 0.2),
        width: cfg.size * 1,
        height: cfg.size * 0.4,
        // fill: 'rgba(255,255,255,1)',
        // stroke: 'rgba(52,142,226,1)',
        fill: origin.nodeColor,
        stroke: origin.nodeColor,
        radius: 3,
      },
    });
  },
  afterDraw: (cfg, group) => {
    const origin = cfg.origin;
    // 内框上
    group.addShape('rect', {
      attrs: {
        x: cfg.x + (cfg.size * 0.1),
        y: cfg.y - (cfg.size * 0.35),
        width: cfg.size * 0.6,
        height: cfg.size * (0.4 / 2),
        fill: 'rgba(74,36,106,1)',
        stroke: 'rgba(74,36,106,1)', // 紫色
        radius: 3,
      },
    });
    // 内框下
    // group.addShape('rect', {
    //   attrs: {
    //     x: cfg.x + (cfg.size * 0.1),
    //     y: cfg.y + (cfg.size * 0.15),
    //     width: cfg.size * 0.6,
    //     height: cfg.size * (0.4 / 2),
    //     fill: 'rgba(255,255,255,1)',
    //     stroke: 'rgba(52,142,226,1)',
    //     radius: 3,
    //   },
    // });
    // 文字上
    group.addShape('text', {
      attrs: {
        text: origin.showValue,
        fill: 'rgba(255,255,255,1)',
        textAlign: 'center',
        textBaseline: 'middle',
        fontSize: 12,
        x: cfg.x + (cfg.size * 0.4),
        y: cfg.y - (cfg.size * 0.25),
      },
    });
    // 文字下
    // group.addShape('text', {
    //   attrs: {
    //     text: '999/1k',
    //     fill: 'rgba(52,142,226,1)',
    //     textAlign: 'center',
    //     textBaseline: 'middle',
    //     fontSize: 12,
    //     x: cfg.x + (cfg.size * 0.4),
    //     y: cfg.y + (cfg.size * 0.25),
    //   },
    // });
    // 服务名称文字
    const labelAttrs = {
      text: origin.value,
      fill: 'rgba(255,255,255,1)',
      textAlign: 'center',
      textBaseline: 'middle',
      fontSize: 15,
      x: cfg.x + (cfg.size * 0),
      y: cfg.y,
    };
    return group.addShape('text', {
      attrs: labelAttrs,
    });
  },
};

const customEdge = {
  draw: (cfg, group) => {
    const points = cfg.points;
    const start = points[0];
    const end = points[points.length - 1];
    const center = {
      x: (start.x + end.x) / 2,
      y: (start.y + end.y) / 2,
    };
    const keyShape = group.addShape('polyline', {
      attrs: {
        points: [
          [start.x, start.y],
          [center.x, start.y],
          [center.x, end.y],
          [end.x, end.y],
        ],
        stroke: 'black',
      },
    });
    return keyShape;
  },
};

const data = {
  nodes: [
    {
      id: 's1',
      value: '申报平台',
    },
    {
      id: 's2',
      value: '申报接口',
    },
    {
      id: 'quota',
      value: 'quota',
    },
    {
      id: 's3',
      value: '任务分发服务',
    },
    {
      id: 's4',
      value: '审单服务',
    },
    {
      id: 's5',
      value: '许可证',
    },
    {
      id: 's6',
      value: '风险',
    },
    {
      id: 's7',
      value: '放行服务',
    },
    {
      id: 's8',
      value: '反馈',
    },
    {
      id: 's9',
      value: '计税',
    },
    {
      id: 'sd',
      value: '减免税',
    },
    {
      id: 'sa',
      value: '仓单',
    },
    {
      id: 'sb',
      value: '放行检查',
    },
    {
      id: 'sc',
      value: '人工审单平台',
    },
  ],
  edges: [
    {
      source: 's1',
      id: 'n1tos1',
      target: 'quota',
    },
    {
      source: 's2',
      id: 'n2tos1',
      target: 'quota',
    },
    {
      source: 'quota',
      id: 's1tonode3',
      target: 's3',
    },
    {
      source: 's3',
      id: 'edge3',
      target: 's4',
    },
    {
      source: 's4',
      id: 'edge4',
      target: 's5',
    },
    {
      source: 's4',
      id: 'edge5',
      target: 's6',
    },
    {
      source: 's4',
      id: 'edge6',
      target: 's7',
    },
    {
      source: 's4',
      id: 'edge7',
      target: 's8',
    },
    {
      source: 's4',
      id: 'edge8',
      target: 's9',
    },
    {
      source: 's4',
      id: 'edge11',
      target: 'sc',
    },
    {
      source: 's7',
      id: 'edge9',
      target: 'sa',
    },
    {
      source: 's7',
      id: 'edge10',
      target: 'sb',
    },
    {
      source: 's9',
      id: 'edge12',
      target: 'sd',
    },
    {
      source: 'sc',
      id: '12to10',
      target: 'sa',
    },
    {
      source: 'sc',
      id: '12to11',
      target: 'sb',
    },
  ],
};

export default {
  dataOrigin,
  data,
  customnodeS,
  customnode1,
  customnode2,
  customEdge,
  customQuota,
};
