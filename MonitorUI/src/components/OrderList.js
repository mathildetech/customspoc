import React from 'react';
import { Table, Input, Button } from 'antd';
import request from '../utils/request';

class OrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      tableData: [],
      columns: [],
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleSearchChange(event) {
    let value = event.target.value;
    value = value.replace(/[，]/ig, ',');
    this.setState({ search: value });
  }

  handleSearch() {
    let columnLength = 0;
    const columns = [{
      title: '订单号',
      dataIndex: 'orderNo',
    }];

    const tableData = [];
    console.log(this.state.search);
    request(`/api/monitorservice/order_logs?order_no=${this.state.search}`).then((orderData) => {
    // request('/order_log.json').then((orderData) => {
      if (orderData.data instanceof Array) {
        orderData.data.forEach((element, i) => {
          const tableDataItem = {
            key: i,
            orderNo: element.orderNO,
          };
          element.history.forEach((elementH, j) => {
            const No = j + 1;
            if (columns.length === 1) {
              columns.push({
                title: `节点${No}`,
                dataIndex: `o${j}`,
              });
            }
            if (j > columnLength) {
              columns.push({
                title: `节点${No}`,
                dataIndex: `o${j}`,
              });
              columnLength += 1;
            }
            const time = new Date(elementH.start.substr(0, elementH.start.length - 1));
            const timeString = time.toLocaleTimeString();
            tableDataItem[`o${j}`] = `${elementH.serviceName}       ${timeString}`;
            // tableDataItem[('t' + j)] = elementH.start;
          });
          tableData.push(tableDataItem);
        });
      }
      this.setState({
        tableData,
        columns,
      });
    });
  }

  render() {
    const locale = {
      emptyText: '请输入订单号搜索',
    };

    return (
      <div>
        <Input placeholder="请输入订单号（多个订单号用逗号分隔）" value={this.state.search} onChange={this.handleSearchChange} style={{ width: '300px', marginRight: '5px', marginBottom: '20px' }} />
        <Button type="primary" icon="search" onClick={this.handleSearch}>查询</Button>
        <Table dataSource={this.state.tableData} columns={this.state.columns} locale={locale} scroll={{ x: true }} />
      </div>
    );
  }
}

export default OrderList;
