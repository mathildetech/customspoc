import React from 'react';
import { Table } from 'antd';
import G6 from '../utils/g6';
import request from '../utils/request';
import { data, customnodeS, customnode2, customEdge, customQuota } from '../constrant';

G6.track(false);

function thousand(num) {
  if (num >= 1000) {
    return (num / 1000).toFixed(1) + 'k';
  }
  return num;
}

function mergeData(serverData, nodeData) {
  let max = 0;
  let min = 0;
  for (let i = 0; i < serverData.length; i++) {
    for (let j = 0; j < nodeData.nodes.length; j++) {
      if (serverData[i].serviceName === nodeData.nodes[j].id) {
        if (serverData[i].servers instanceof Array) {
          let orderProcessed = 0;
          let orderInque = 0;
          let orderTotal = 0;
          let total = 0;
          while (total < serverData[i].servers.length) {
            const sorver = serverData[i].servers[total].order;
            orderProcessed += sorver.processed;
            orderInque += sorver.inQueue;
            orderTotal += sorver.processed + sorver.inQueue;
            total += 1;
          }
          if (orderTotal > max || max === 0) {
            max = orderTotal;
          }
          if (orderTotal < min || min === 0) {
            min = orderTotal;
          }
          nodeData.nodes[j].showValue = thousand(orderInque) + '/' + thousand(orderProcessed);
          nodeData.nodes[j].orderTotal = orderTotal;
        }
        nodeData.nodes[j].servers = serverData[i].servers;
      }
    }
  }
  const step = (max - min) / 3;
  const arrStep = [min + step, max - step];
  for (let k = 0; k < nodeData.nodes.length; k++) {
    const sval = nodeData.nodes[k];
    if (sval.orderTotal > arrStep[0]) {
      sval.nodeColor = 'rgb(22,165,63)';
    } else if (sval.orderTotal > arrStep[1]) {
      sval.nodeColor = 'rgb(68,113,40)';
    } else {
      sval.nodeColor = 'rgb(154,202,124)';
    }
  }
}

class OrderFigure extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      net: {},
    };
  }

  componentDidMount() {
    request('/api/monitorservice/service_logs').then((serverData) => {
    // request('/service_log.json').then((serverData) => {
      const Util = G6.Util;
      const margin = 50;
      const height = 800 - (2 * margin);
      const width = 400 - (2 * margin);
      const Layout = G6.Layout;
      // 设置数据
      const layout = new Layout.Flow({
        nodes: Util.clone(data.nodes),
        edges: Util.clone(data.edges),
        calculationTimes: 2,
      });
      data.nodes.map((node) => {
        if (node.id === 'quota') {
          node.shape = 'customQuota';
        } else {
          node.shape = 'customNode2';
        }
        return null;
      });
      // 自定义节点和边
      G6.registNode('customNode2', customnode2);
      G6.registNode('customNodeS', customnodeS);
      G6.registNode('customQuota', customQuota);
      G6.registEdge('customeEdge', customEdge);
      // 配置G6画布
      const net = new G6.Net({
        id: 'c1',      // 容器ID
        fitView: 'cc',
        // width: 800,    // 画布宽
        height: 500,    // 画布高
        modes: {
          default: [],
          add: [],
        },
      });
      this.setState({
        net,
      });
      if (serverData.data instanceof Array) {
        mergeData(serverData.data, data);
      }
      // 载入数据
      net.source(data.nodes, data.edges);
      // 生成自定义节点
      net.node()
        .label((obj) => {
          return obj.name;
        })
        .style({
          fillOpacity: 1,
        })
        .size(100);
      net.edge()
        .shape('HVH')
        .style({
          stroke: 'rgba(52,142,226,1)',
          arrow: true,
        });
      net.on('click', (ev) => {
        ev.domEvent.preventDefault();
        if (G6.Util.isNode(ev.item)) {
          if (ev.item.get('model').servers) {
            this.setState({
              dataSource: ev.item.get('model').servers.map((el) => {
                return {
                  serverName: el.serverName,
                  serverStatus: (el.serverStatus === 'ON_LINE' ? '在线' : '离线'),
                  cpu: el.cpu,
                  memory: `${el.memory.available} / ${el.memory.total}`,
                  disk: `${el.disk.available} / ${el.disk.total}`,
                  order: `${el.order.inQueue} / ${el.order.processed}`,
                };
              }),
            });
          } else {
            this.setState({
              dataSource: [],
            });
          }
        }
      });
      // 渲染关系图
      net.render();
      layout.onNodeChange = (id, point) => {
        const node = net.find(id);
        net.update(node, {
          x: (point.y * height * 1.2) - (8 * margin),
          y: (point.x * width * 1.5) - (4.5 * margin),
        });
        net.refresh();
      };
      layout.start();
    });
    setInterval(() => {
      request('/api/monitorservice/service_logs').then((serverData) => {
        if (serverData.data instanceof Array) {
          mergeData(serverData.data, data);
        }
        this.state.net.changeData(data.nodes, data.edges);
      });
    }, 5 * 1000);
  }

  render() {
    const locale = {
      emptyText: '请选择一个节点查看数据',
    };

    const columns = [{
      title: '节点',
      dataIndex: 'serverName',
      key: 'serverName',
    }, {
      title: '状态',
      dataIndex: 'serverStatus',
      key: 'serverStatus',
    }, {
      title: 'CPU',
      dataIndex: 'cpu',
      key: 'cpu',
    }, {
      title: '内存',
      dataIndex: 'memory',
      key: 'memory',
    }, {
      title: '磁盘',
      dataIndex: 'disk',
      key: 'disk',
    }, {
      title: '订单',
      dataIndex: 'order',
      key: 'order',
    }];
    return (
      <div>
        <div id="c1" />
        <Table dataSource={this.state.dataSource} columns={columns} locale={locale} rowKey="serverName" />
      </div>
    );
  }
}

export default OrderFigure;
