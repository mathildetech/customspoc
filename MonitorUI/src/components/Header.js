import React from 'react';
import { Menu } from 'antd';
import { Link } from 'dva/router';
import styles from '../style/Figure.css';

function Header({ location }) {
  return (
    <div className="ant-layout-header">
      <div className={styles.logo} />
      <Menu
        theme="dark"
        mode="horizontal"
        selectedKeys={[location.pathname]}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="/">
          <Link to="/">系统状态监控</Link>
        </Menu.Item>
        <Menu.Item key="/page1">
          <Link to="/page1">业务处理监控</Link>
        </Menu.Item>
        <Menu.Item key="/page2">
          <Link to="/page2">业务数据追踪</Link>
        </Menu.Item>
      </Menu>
    </div>
  );
}

export default Header;
