import React from 'react';
import { Layout } from 'antd';
import { connect } from 'dva';
import Header from '../components/Header';
import Figure from '../components/Figure';

const { Content, Footer } = Layout;

function Figure1({ location }) {
  return (
    <Layout className="layout">
      <Header location={location} />
      <Content style={{ padding: '25px 50px 0' }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
          <Figure />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        Copyright © 2017 by Alauda
      </Footer>
    </Layout>
  );
}

export default connect()(Figure1);
