import React from 'react';
import { Layout } from 'antd';
import { connect } from 'dva';
import Header from '../components/Header';
import OrderList from '../components/OrderList';

const { Content, Footer } = Layout;

function Figure2({ location }) {
  return (
    <Layout className="layout">
      <Header location={location} />
      <Content style={{ padding: '25px 50px 0' }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
          <OrderList />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        Copyright © 2017 by Alauda
      </Footer>
    </Layout>
  );
}

export default connect()(Figure2);
