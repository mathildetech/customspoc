package com.alauda.microservice.hystrix;

import java.util.ArrayList;
import java.util.List;

public class ApiError {
	private final String result;
    private final String message;
    private final List<ApiSubError> errors;

    public ApiError(String result, String message) {
        this.result = result;
        this.message = message;
        this.errors = new ArrayList<>();
    }

	public String getResult() {
		return result;
	}

	public String getMessage() {
		return message;
	}
	public void addApiSubError(String field, String message) {
		this.errors.add(new ApiSubError(field, message));
	}
}

class ApiSubError {
	private String field;
	private String message;
	
	public ApiSubError(String field, String message) {
			this.field = field;
			this.message = message;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}
	
}
