package com.alauda.microservice.hystrix;

import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.alauda.microservice.Result;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class CommandThatFailsFast extends HystrixCommand<ResponseEntity<Object>> {

    private final boolean throwException;
    static Random random = new Random();
    public CommandThatFailsFast(boolean throwException) {
        super(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"));
        this.throwException = throwException;
    }

    /**
     * 这个方法里面封装了正常的逻辑，我们可以传入正常的业务逻辑
     * 
     * @return
     * @throws Exception
     */
    @Override
    protected ResponseEntity<Object> run() throws Exception {

        // 如果为true，这个方法丢出异常，如果为false就返回字符串
        if (throwException) {
            throw new RuntimeException("failure from CommandThatFailsFast");
        } else {
        		if (random.nextInt(100) < 75) {
        			return new ResponseEntity<Object>(new Result(true), HttpStatus.OK);
        		} else {
        			return new ResponseEntity<Object>(new Result(false), HttpStatus.BAD_REQUEST);
        		}
            
        }
    }

    /**
     * 这个方法中定义了出现异常时, 默认返回的值(相当于服务的降级)。
     * 
     * @return
     */
    @Override
    protected ResponseEntity<Object> getFallback() {
    		ApiError apiError = new ApiError("false", "service hystrix error");
    		apiError.addApiSubError("unknown", "current service not available");
    		return new ResponseEntity<Object>(apiError, HttpStatus.SERVICE_UNAVAILABLE);
    }

}
