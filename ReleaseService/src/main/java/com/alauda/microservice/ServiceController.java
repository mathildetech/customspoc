package com.alauda.microservice;

import java.util.Random;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alauda.microservice.hystrix.CommandThatFailsFast;

@RestController
public class ServiceController {
	static Random random = new Random();
    @RequestMapping("/release_check")
    public ResponseEntity<Object> releaseCheck(@RequestParam(value="orderId") String name) {
    		return new CommandThatFailsFast(random.nextInt(100) < 75).execute();
    }
}
