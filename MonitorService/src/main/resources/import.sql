INSERT INTO "CUSTOMER" (ID, NAME, EMAIL, CREATED_DATE) VALUES(1, 'mkyong','111@yahoo.com', TO_DATE('2017-02-11', 'yyyy-mm-dd'));
INSERT INTO "CUSTOMER" (ID, NAME, EMAIL, CREATED_DATE) VALUES(2, 'yflow','222@yahoo.com', TO_DATE('2017-02-12', 'yyyy-mm-dd'));
INSERT INTO "CUSTOMER" (ID, NAME, EMAIL, CREATED_DATE) VALUES(3, 'zilap','333@yahoo.com', TO_DATE('2017-02-13', 'yyyy-mm-dd'));

INSERT INTO "ORDER_LOG" (ID, ORDERNO, RESULT, REASON, SERVICE_NAME, START_TIME, END_TIME) VALUES(1, 'ZILAP','成功', NULL, 'SERVICE1', TO_DATE('2017-10-13 04:30:00', 'yyyy-mm-dd hh:mi:ss'), TO_DATE('2017-10-14 04:30:01', 'yyyy-mm-dd hh:mi:ss'));
INSERT INTO "ORDER_LOG" (ID, ORDERNO, RESULT, REASON, SERVICE_NAME, START_TIME, END_TIME) VALUES(2, 'ZILAP','成功', NULL, 'SERVICE2', TO_DATE('2017-10-11 04:30:50', 'yyyy-mm-dd hh:mi:ss'), TO_DATE('2017-10-14 04:31:00', 'yyyy-mm-dd hh:mi:ss'));
INSERT INTO "ORDER_LOG" (ID, ORDERNO, RESULT, REASON, SERVICE_NAME, START_TIME, END_TIME) VALUES(3, 'O-001','成功', NULL, 'SERVICE1', TO_DATE('2017-10-16 04:30:00', 'yyyy-mm-dd hh:mi:ss'), TO_DATE('2017-10-16 04:32:01', 'yyyy-mm-dd hh:mi:ss'));
INSERT INTO "ORDER_LOG" (ID, ORDERNO, RESULT, REASON, SERVICE_NAME, START_TIME, END_TIME) VALUES(4, 'O-001','成功', NULL, 'SERVICE2', TO_DATE('2017-10-17 04:30:50', 'yyyy-mm-dd hh:mi:ss'), TO_DATE('2017-10-17 04:33:00', 'yyyy-mm-dd hh:mi:ss'));
