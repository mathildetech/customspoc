/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alauda.microservice.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alauda.microservice.dao.CustomerRepository;
import com.alauda.microservice.dao.OrderLogRepository;
import com.alauda.microservice.domain.Customer;
import com.alauda.microservice.domain.Disk;
import com.alauda.microservice.domain.Memory;
import com.alauda.microservice.domain.Order;
import com.alauda.microservice.domain.OrderHistory;
import com.alauda.microservice.domain.OrderLog;
import com.alauda.microservice.domain.Result;
import com.alauda.microservice.domain.Server;
import com.alauda.microservice.domain.ServerStatus;
import com.alauda.microservice.domain.Service;
import com.alauda.microservice.hystrix.CommandThatFailsFast;

@RestController
@Component
public class IndexController {
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	OrderLogRepository orderLogRepository;

	static List<Service> services;
	static Random random = new Random();
	
	static {
		services = new ArrayList<Service>();
		
		//////////////////////////////////////////////////
		Memory s11_m = new Memory("1GB", "8GB");
		Disk s11_d = new Disk("20GB", "100GB");
		Order s11_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s11 = new Server("s11", ServerStatus.ON_LINE, "60%", s11_m, s11_d, s11_o);
		
		Memory s12_m = new Memory("1GB", "8GB");
		Disk s12_d = new Disk("20GB", "100GB");
		Order s12_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s12 = new Server("s12", ServerStatus.ON_LINE, "60%", s12_m, s12_d, s12_o);		
		
		Service s1 = new Service();
		s1.setServiceName("s1");
		s1.addServers(s11, s12);
		
		services.add(s1);
		//////////////////////////////////////////////////
		
		Memory s21_m = new Memory("1GB", "8GB");
		Disk s21_d = new Disk("20GB", "100GB");
		Order s21_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s21 = new Server("s21", ServerStatus.ON_LINE, "60%", s21_m, s21_d, s21_o);
		
		Memory s22_m = new Memory("1GB", "8GB");
		Disk s22_d = new Disk("20GB", "100GB");
		Order s22_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s22 = new Server("s22", ServerStatus.ON_LINE, "60%", s22_m, s22_d, s22_o);		
		
		Service s2 = new Service();
		s2.setServiceName("s2");
		s2.addServers(s21, s22);

		services.add(s2);
		//////////////////////////////////////////////////
		Memory s31_m = new Memory("1GB", "8GB");
		Disk s31_d = new Disk("20GB", "100GB");
		Order s31_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s31 = new Server("s31", ServerStatus.ON_LINE, "60%", s31_m, s31_d, s31_o);
		
		Memory s32_m = new Memory("1GB", "8GB");
		Disk s32_d = new Disk("20GB", "100GB");
		Order s32_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s32 = new Server("s32", ServerStatus.ON_LINE, "60%", s32_m, s32_d, s32_o);		
		
		Service s3 = new Service();
		s3.setServiceName("s3");
		s3.addServers(s31, s32);
		
		services.add(s3);

//////////////////////////////////////////////////
		Memory s41_m = new Memory("1GB", "8GB");
		Disk s41_d = new Disk("20GB", "100GB");
		Order s41_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s41 = new Server("s41", ServerStatus.ON_LINE, "60%", s41_m, s41_d, s41_o);
		
		Memory s42_m = new Memory("1GB", "8GB");
		Disk s42_d = new Disk("20GB", "100GB");
		Order s42_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s42 = new Server("s42", ServerStatus.ON_LINE, "60%", s42_m, s42_d, s42_o);		
		
		Service s4 = new Service();
		s4.setServiceName("s4");
		s4.addServers(s41, s42);
		
		services.add(s4);
		
//////////////////////////////////////////////////
		Memory s51_m = new Memory("1GB", "8GB");
		Disk s51_d = new Disk("20GB", "100GB");
		Order s51_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s51 = new Server("s51", ServerStatus.ON_LINE, "60%", s51_m, s51_d, s51_o);
		
		Memory s52_m = new Memory("1GB", "8GB");
		Disk s52_d = new Disk("20GB", "100GB");
		Order s52_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s52 = new Server("s52", ServerStatus.ON_LINE, "60%", s52_m, s52_d, s52_o);		
		
		Service s5 = new Service();
		s5.setServiceName("s5");
		s5.addServers(s51, s52);
		
		services.add(s5);
		
//////////////////////////////////////////////////
		Memory s61_m = new Memory("1GB", "8GB");
		Disk s61_d = new Disk("20GB", "100GB");
		Order s61_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s61 = new Server("s61", ServerStatus.ON_LINE, "60%", s61_m, s61_d, s61_o);
		
		Memory s62_m = new Memory("1GB", "8GB");
		Disk s62_d = new Disk("20GB", "100GB");
		Order s62_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s62 = new Server("s62", ServerStatus.ON_LINE, "60%", s62_m, s62_d, s62_o);		
		
		Service s6 = new Service();
		s6.setServiceName("s6");
		s6.addServers(s61, s62);
		
		services.add(s6);
		
//////////////////////////////////////////////////
		Memory s71_m = new Memory("1GB", "8GB");
		Disk s71_d = new Disk("20GB", "100GB");
		Order s71_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s71 = new Server("s71", ServerStatus.ON_LINE, "60%", s71_m, s71_d, s71_o);
		
		Memory s72_m = new Memory("1GB", "8GB");
		Disk s72_d = new Disk("20GB", "100GB");
		Order s72_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s72 = new Server("s72", ServerStatus.ON_LINE, "60%", s72_m, s72_d, s72_o);		
		
		Service s7 = new Service();
		s7.setServiceName("s7");
		s7.addServers(s71, s72);
		
		services.add(s7);
		
//////////////////////////////////////////////////
		Memory s81_m = new Memory("1GB", "8GB");
		Disk s81_d = new Disk("20GB", "100GB");
		Order s81_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s81 = new Server("s81", ServerStatus.ON_LINE, "60%", s81_m, s81_d, s81_o);
		
		Memory s82_m = new Memory("1GB", "8GB");
		Disk s82_d = new Disk("20GB", "100GB");
		Order s82_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s82 = new Server("s82", ServerStatus.ON_LINE, "60%", s82_m, s82_d, s82_o);		
		
		Service s8 = new Service();
		s8.setServiceName("s8");
		s8.addServers(s81, s82);
		
		services.add(s8);

//////////////////////////////////////////////////
		Memory s91_m = new Memory("1GB", "8GB");
		Disk s91_d = new Disk("20GB", "100GB");
		Order s91_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s91 = new Server("s91", ServerStatus.ON_LINE, "60%", s91_m, s91_d, s91_o);
		
		Memory s92_m = new Memory("1GB", "8GB");
		Disk s92_d = new Disk("20GB", "100GB");
		Order s92_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server s92 = new Server("s92", ServerStatus.ON_LINE, "60%", s92_m, s92_d, s92_o);		
		
		Service s9 = new Service();
		s9.setServiceName("s9");
		s9.addServers(s91, s92);
		
		services.add(s9);

//////////////////////////////////////////////////
		Memory sa1_m = new Memory("1GB", "8GB");
		Disk sa1_d = new Disk("20GB", "100GB");
		Order sa1_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sa1 = new Server("sa1", ServerStatus.ON_LINE, "60%", sa1_m, sa1_d, sa1_o);
		
		Memory sa2_m = new Memory("1GB", "8GB");
		Disk sa2_d = new Disk("20GB", "100GB");
		Order sa2_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sa2 = new Server("sa2", ServerStatus.ON_LINE, "60%", sa2_m, sa2_d, sa2_o);		
		
		Service sa = new Service();
		sa.setServiceName("sa");
		sa.addServers(sa1, sa2);
		
		services.add(sa);
		
//////////////////////////////////////////////////
		Memory sb1_m = new Memory("1GB", "8GB");
		Disk sb1_d = new Disk("20GB", "100GB");
		Order sb1_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sb1 = new Server("sb1", ServerStatus.ON_LINE, "60%", sb1_m, sb1_d, sb1_o);
		
		Memory sb2_m = new Memory("1GB", "8GB");
		Disk sb2_d = new Disk("20GB", "100GB");
		Order sb2_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sb2 = new Server("sb2", ServerStatus.ON_LINE, "60%", sb2_m, sb2_d, sb2_o);		
		
		Service sb = new Service();
		sb.setServiceName("sb");
		sb.addServers(sb1, sb2);
		
		services.add(sb);

//////////////////////////////////////////////////
		Memory sc1_m = new Memory("1GB", "8GB");
		Disk sc1_d = new Disk("20GB", "100GB");
		Order sc1_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sc1 = new Server("sc1", ServerStatus.ON_LINE, "60%", sc1_m, sc1_d, sc1_o);
		
		Memory sc2_m = new Memory("1GB", "8GB");
		Disk sc2_d = new Disk("20GB", "100GB");
		Order sc2_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sc2 = new Server("sc2", ServerStatus.ON_LINE, "60%", sc2_m, sc2_d, sc2_o);		
		
		Service sc = new Service();
		sc.setServiceName("sc");
		sc.addServers(sc1, sc2);
		
		services.add(sc);
		
//////////////////////////////////////////////////
		Memory sd1_m = new Memory("1GB", "8GB");
		Disk sd1_d = new Disk("20GB", "100GB");
		Order sd1_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sd1 = new Server("sd1", ServerStatus.ON_LINE, "60%", sd1_m, sd1_d, sd1_o);
		
		Memory sd2_m = new Memory("1GB", "8GB");
		Disk sd2_d = new Disk("20GB", "100GB");
		Order sd2_o = new Order(random.nextInt(25), 1000 + random.nextInt(1000));
		Server sd2 = new Server("sd2", ServerStatus.ON_LINE, "60%", sd2_m, sd2_d, sd2_o);		
		
		Service sd = new Service();
		sd.setServiceName("sd");
		sd.addServers(sd1, sd2);
		
		services.add(sd);
		
//////////////////////////////////////////////////
	
	}
	
	@RequestMapping("/customers")
	@Transactional(readOnly = true)
	public List<Customer> index() {
		List<Customer> customers = new ArrayList<>();
		for (Customer customer : customerRepository.findAll()) {
			customers.add(customer);
			System.out.println(customer);
		}
		
		return customers;
	}

    @RequestMapping("/hystrix")
    public String hello() {
        // 用随机数来模拟错误, 这里让正确率高一些
        return new CommandThatFailsFast(random.nextInt(100) < 75).execute();
    }

	@RequestMapping(method = RequestMethod.GET, path = "/order_logs")
	@Transactional(readOnly = true)
	public List<OrderHistory> findOrderLogByNo(@RequestParam List<String> order_no) {
		logger.info("order_nos={}", order_no.stream().collect(Collectors.joining(",", "[", "]")));
		return order_no.stream().map(no -> orderLogRepository.findByOrderNO(no).collect(Collectors.toList())).map(logs -> OrderLog.valueOf(logs)).collect(Collectors.toList());
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/order_logs")
	@Transactional
	public Result insertOrderLog(@RequestBody OrderLog orderLog) {
		
		System.out.println(orderLog);
		orderLogRepository.save(orderLog);
		return new Result();
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/service_logs")
	@Transactional(readOnly = true)
	public List<Service> serviceLogs() {
		return services;
	}

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
	    	services.forEach(s -> {
	    		s.getServers().forEach(r -> r.random());
	    	});
    }
}
