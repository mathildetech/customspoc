package com.alauda.microservice.domain;

import java.util.List;

public class OrderHistory {
	
	private String orderNO;
	
	private List<OrderLog> history;
	
	public String getOrderNO() {
		return orderNO;
	}

	public void setOrderNO(String orderNO) {
		this.orderNO = orderNO;
	}

	public List<OrderLog> getHistory() {
		return history;
	}

	public void setHistory(List<OrderLog> history) {
		this.history = history;
	}

	@Override
	public String toString() {
		return "OrderHistory [orderNO=" + orderNO + ", history=" + history + "]";
	}
	
}
