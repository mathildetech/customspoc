package com.alauda.microservice.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ORDER_SEQ")
    @SequenceGenerator(sequenceName = "orderlog_seq", allocationSize = 1, name = "ORDER_SEQ")
    @JsonIgnore
    Long id;

    @JsonIgnore
	private String orderNO;
	private String result;
	private String reason;
	private String serviceName;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss'T'")
	@Column(name = "START_TIME")
	private Date start;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss'T'")
	@Column(name = "END_TIME")
	private Date end;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrderNO() {
		return orderNO;
	}
	public void setOrderNO(String orderNO) {
		this.orderNO = orderNO;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	
	public static OrderHistory valueOf(List<OrderLog> orderLogs) {
		if (orderLogs.size() == 0) {
			return null;
		}
		else {
			OrderHistory orderHistory = new OrderHistory();
			orderHistory.setHistory(orderLogs);
			
			orderHistory.setOrderNO(orderLogs.get(1).getOrderNO());
			return orderHistory;
		}
	}
	
	@Override
	public String toString() {
		return "OrderLog [id=" + id + ", orderNO=" + orderNO + ", result=" + result + ", reason=" + reason
				+ ", serviceName=" + serviceName + ", start=" + start + ", end=" + end + "]";
	}
	
}
