package com.alauda.microservice.domain;

public class Result {
	boolean result = true;
	
	public Result(){
		
	}
	
	public Result(boolean result){
		this.result = result;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
	
}
