package com.alauda.microservice.domain;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Memory {
	String total;
	String available;
	
	@JsonIgnore
	Random random = new Random();
	
	public Memory() {
	}

	public Memory(String available, String total) {
		this.available = available;
		this.total = total;
	}
	
	@JsonIgnore
	public void random() {
		available = (3 + random.nextInt(4)) + "GB";
	}

}
