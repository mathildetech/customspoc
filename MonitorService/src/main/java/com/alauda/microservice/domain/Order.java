package com.alauda.microservice.domain;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Order {
	int processed;
	int inQueue;
	
	@JsonIgnore
	Random random = new Random();
	
	public Order() {}
	
	public Order(int inQueue, int processed) {
		this.processed = processed;
		this.inQueue = inQueue;
	}
	
	@JsonIgnore
	public void random() {
		processed += random.nextInt(5);
		inQueue = 5 + random.nextInt(20);
	}
}
