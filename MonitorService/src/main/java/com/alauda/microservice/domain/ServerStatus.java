package com.alauda.microservice.domain;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;

public enum ServerStatus {
	ON_LINE, OFF_LINE;
	
	@JsonIgnore
	static Random random = new Random();
	
	@JsonIgnore
	public static ServerStatus random() {
		if (random.nextInt(100) < 90) {
			return ON_LINE;
		}
		else {
			return OFF_LINE;
		}
	}
}
