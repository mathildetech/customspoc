package com.alauda.microservice.domain;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Server {
	String serverName;
	ServerStatus serverStatus = ServerStatus.ON_LINE;	
	String cpu = "60%";
	Memory memory;
	Disk disk;
	Order order;

	@JsonIgnore
	Random random = new Random();
	
	public Server() {}
	
	public Server(String serverName, ServerStatus serverStatus, String cpu, Memory memory, Disk disk, Order order) {
		this.serverName = serverName;
		this.serverStatus = serverStatus;
		this.cpu = cpu;
		this.memory = memory;
		this.disk = disk;
		this.order = order;
	}
	
	@JsonIgnore
	public void random() {
		cpu = (40 + random.nextInt(60)) + "%";
		serverStatus = ServerStatus.random();
		memory.random();
		disk.random();
		order.random();
	}

}
