package com.alauda.microservice.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Service {
	String serviceName;
	
	List<Server> servers = new ArrayList<>();
	
	public void addServers(Server... s) {
		servers.addAll(Arrays.asList(s));
	}

	public void setServiceName(String string) {
		// TODO Auto-generated method stub
		this.serviceName = string;
	}

	public List<Server> getServers() {
		// TODO Auto-generated method stub
		return servers;
	}
}
