package com.alauda.microservice.domain;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Disk {
	String total;
	String available;
	
	@JsonIgnore
	Random random = new Random();
	
	public Disk() {}
	
	public Disk(String available, String total) {
		this.available = available;
		this.total = total;
	}

	@JsonIgnore
	public void random() {
		available = (10 + random.nextInt(20)) + "GB";
	}

}
