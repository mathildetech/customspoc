package com.alauda.microservice.dao;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.alauda.microservice.domain.OrderLog;

public interface OrderLogRepository extends CrudRepository<OrderLog, Long> {

    @Query("select o from OrderLog o where o.orderNO = :orderNO order by o.start")
    Stream<OrderLog> findByOrderNO(@Param("orderNO") String orderNO);

}
