package com.alauda.microservice.bean;

public class OrderTax {
	private String OrderNo;
	private String Tax;
	private String Final;
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getTax() {
		return Tax;
	}
	public void setTax(String tax) {
		Tax = tax;
	}
	public String getFinal() {
		return Final;
	}
	public void setFinal(String final1) {
		Final = final1;
	}
}
