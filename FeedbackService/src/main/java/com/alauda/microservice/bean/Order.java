package com.alauda.microservice.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat.Value;
import com.fasterxml.jackson.annotation.JsonIgnore;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {
	private long orderId;
	private Company company;
	private OrderStatus orderStatus;
	private Custom custom;
	private Storage storage;
	private List<Item> items ;
	public Order() {}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		orderId = orderId;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Custom getCustom() {
		return custom;
	}
	public void setCustom(Custom custom) {
		this.custom = custom;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	@Override
	@JsonIgnore
    public String toString() {
        return "Order{" +
                "OrderId=" + orderId + 
                "storage=" + storage +
                "status=" + orderStatus +
                '}';
    }
}
