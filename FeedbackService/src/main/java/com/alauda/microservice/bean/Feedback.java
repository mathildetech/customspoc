package com.alauda.microservice.bean;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Feedback {

	private long orderId;
    private String message;
    private String createBy;
    private String createAt;

    public Feedback() {}
    
    public Feedback(long orderNO, String message, String createBy, String createAt) {
        this.orderId = orderNO;
        this.message = message;
        this.createBy = createBy;
        this.createAt = createAt;
    }

    public long getOrderId() {
        return orderId;
    }

    public String getMessage() {
        return message;
    }
    
    public String getCreatedBy() {
        return createBy;
    }

    public String getCreatedAt() {
        return createAt;
    }
    
    public void setOrderId(long orderNO) {
        this.orderId = orderNO;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public void setCreatedBy(String createBy) {
        this.createBy = createBy;
    }

    public void setCreatedAt(String createAt) {
        this.createAt = createAt;
    }
    
    @Override
    public String toString() {
        return "Feedback{" +
                "OrderNO='" + orderId + '\'' +
                ", message=" + message +
                '}';
    }
}
