package com.alauda.microservice.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Storage {
	private String StorageCode;
	private String StorageName;
	private String StorageType;
	public Storage () {}
	public String getStorageCode() {
		return StorageCode;
	}
	public void setStorageCode(String storageCode) {
		StorageCode = storageCode;
	}
	public String getStorageName() {
		return StorageName;
	}
	public void setStorageName(String storageName) {
		StorageName = storageName;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	@Override
    public String toString() {
        return "Storage{" +
                "StorageCode=" + StorageCode + 
                "StorageName=" + StorageName + 
                "StorageType=" + StorageType +
                '}';
    }
}
