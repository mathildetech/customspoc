package com.alauda.microservice.bean;

public class Release {

    private final String orderNO;
    private final String message;
    private final String createBy;
    private final String createAt;

    public Release(String orderNO, String message, String createBy, String createAt) {
        this.orderNO = orderNO;
        this.message = message;
        this.createBy = createBy;
        this.createAt = createAt;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public String getMessage() {
        return message;
    }
    
    public String getCreatedBy() {
        return createBy;
    }

    public String getCreatedAt() {
        return createAt;
    }
}
