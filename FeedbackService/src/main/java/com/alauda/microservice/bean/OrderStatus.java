package com.alauda.microservice.bean;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class OrderStatus {
//	DECLARE("未申报", 1), UNDECLARE("申报", 2), BACK("退单", 3), EXEC("布控", 4), RELEASE("放行", 5);
	private int statusCode;
	private String statusName;

	public OrderStatus () {}
	public OrderStatus (String statusName, int statusCode) {
		this.statusName = statusName;
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	@Override
	@JsonIgnore
	public String toString() {
        return "status{" +
                "statusCode=" + statusCode + 
                "statusName=" + statusName +
                '}';
    }
	
}
