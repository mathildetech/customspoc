package com.alauda.microservice.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class StorageResult {
    private Boolean result;
    private String message;
    
    public String getMessage() {
		return message;
	}

	public StorageResult(Boolean result, String message) {
		super();
		this.result = result;
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StorageResult() {}
    
    public StorageResult(Boolean result) {
    		this.result = result;
    }
	public Boolean getResult() {
		return result;
	}
	@Override
	@JsonIgnore
	public String toString() {
		return "{'result': " + result + "}";
	}
}
