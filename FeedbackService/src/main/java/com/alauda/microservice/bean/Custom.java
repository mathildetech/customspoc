package com.alauda.microservice.bean;

public class Custom {
	private String CustomCode;
	private String CustomName;
	public Custom () {}
	public String getCustomCode() {
		return CustomCode;
	}
	public void setCustomCode(String customCode) {
		CustomCode = customCode;
	}
	public String getCustomName() {
		return CustomName;
	}
	public void setCustomName(String customName) {
		CustomName = customName;
	}
	
}
