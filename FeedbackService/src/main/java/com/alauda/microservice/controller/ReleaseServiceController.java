package com.alauda.microservice.controller;

import java.util.Map;
import java.util.Random;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.springframework.web.bind.annotation.*;
import java.time.ZonedDateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.alauda.microservice.PostOrderLog;
import com.alauda.microservice.PostRequest;
import com.alauda.microservice.bean.Feedback;
import com.alauda.microservice.bean.Order;
import com.alauda.microservice.bean.OrderStatus;
import com.alauda.microservice.bean.Release;
import com.alauda.microservice.bean.StorageResult;
import com.alauda.microservice.exceptionhandling.EntityUnvalidException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.System;

@RestController
@RequestMapping("/release")
public class ReleaseServiceController {
	
	private final Map <String, Release> releasebackMap = new HashMap<String, Release>();
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	static Random random = new Random();
	private Date start = new Date();
	
	@Value("${alauda.service.storage_url}")
	String STORAGE_URL;
	
	@Value("${alauda.service.monitor_url}")
	String MONITOR_URL;
	
	@Value("${microsoft.service.tax}")
	String TAX_URL;
	
	@Value("${microsoft.service.reduce_tax}")
	String REDUCE_TAX_URL;
	
	@Value("${microsoft.service.licence}")
	String LICENCE_URL;
	
	@Value("${microsoft.service.risk}")
	String RISK_URL;
	
	RestTemplate restTemplate = new RestTemplate();
	
    @PostMapping(value = "/validate")
    public ResponseEntity<Object> validate(@RequestBody Order order) throws EntityUnvalidException{
		String orderNO = Long.toString(order.getOrderId());
		if (orderNO == null) {
			PostOrderLog.post(MONITOR_URL, order, false, start, "order no invalid");
			throw new EntityUnvalidException();
		} else {
			try {
				StorageResult result = validateFields(order);
				if (result.getResult()) {
					PostOrderLog.post(MONITOR_URL, order, true, start, "");
					return new ResponseEntity<Object>(new StorageResult(true), HttpStatus.OK);
				} else {
					PostOrderLog.post(MONITOR_URL, order, false, start, result.getMessage());
					throw new EntityUnvalidException();
				}
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
				PostOrderLog.post(MONITOR_URL, order, false, start, t.getMessage());
				throw t;
			}
		}
    }
    
    @PostMapping(value = "/process")
    public ResponseEntity<Object> process(@RequestBody Order order)  throws EntityUnvalidException{
    		if (order != null) {
    			order = changeStatus(order);
    			PostOrderLog.post(MONITOR_URL, order, true, start, "");
        		return new ResponseEntity<Object>(order, HttpStatus.OK);
    	    	} else {
    	    		PostOrderLog.post(MONITOR_URL, order, false, start, "storage invalid");
    	    		throw new EntityUnvalidException();
    	    	}
    }
    
    private StorageResult validateFields(Order order) {
    		RestTemplate restTemplate = new RestTemplate();
    		log.error("STORAGE_URL---------------" + STORAGE_URL);
    		System.out.println("STORAGE_URL---------------" + STORAGE_URL);
    		boolean checkStorage = PostRequest.postRequest(STORAGE_URL + "/storage/post_validate", order);
    		if (checkStorage == false) {
    			return new StorageResult(false, "short of storage");
    		}
    		
    		if (checkStorage == false) {
    			return new StorageResult(false, "tax invalid");
    		}
    		if (TAX_URL != null) {
    			System.out.println("STORAGE_URL---------------" + TAX_URL);
	    		boolean checkTax = PostRequest.postMS(TAX_URL + "/api/orders", order, 0);
	    		if (checkTax == false) {
	    			return new StorageResult(false, "tax invalid");
	    		}
	    		System.out.println("checkTax---------------" + checkTax);
    		}
    		if (REDUCE_TAX_URL != null) {
	    		boolean checkReduceTax = PostRequest.postMS(REDUCE_TAX_URL + "/api/orders", order, 0);
	    		if (checkReduceTax == false) {
	    			return new StorageResult(false, "reduce tax invalid");
	    		}
    		}
    		if (LICENCE_URL != null) {
	    		boolean checkLicence = PostRequest.postMS(LICENCE_URL + "/api/orders", order, 0);
	    		if (checkLicence == false) {
	    			return new StorageResult(false, "no licence");
	    		}
    		}
    		if (RISK_URL != null) {
	    		boolean checkRisk = PostRequest.postMS(RISK_URL + "/api/orders", order, 0);
	    		if (checkRisk == false) {
	    			return new StorageResult(false, "too risk");
	    		}
    		}
    		return new StorageResult(true, " ");
    }
    
    private Order changeStatus(Order order) {
    		OrderStatus status = new OrderStatus("已放行", 5);
		order.setOrderStatus(status);
		return order;
    }
}
