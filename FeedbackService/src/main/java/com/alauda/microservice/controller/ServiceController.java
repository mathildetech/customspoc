package com.alauda.microservice.controller;

import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alauda.microservice.bean.Feedback;
import com.alauda.microservice.bean.StorageResult;
import com.alauda.microservice.exceptionhandling.EntityUnvalidException;

import java.time.ZonedDateTime;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

@RestController
public class ServiceController {
	private final Map <String, Feedback> feedbackMap = new HashMap<String, Feedback>();
	
    @RequestMapping(value = "/feedback", method = RequestMethod.GET)
    public Feedback feedbacking(@RequestParam(value="orderId") String orderNO) throws EntityUnvalidException{
       Feedback feed = feedbackMap.get(orderNO);
       if (feed == null) {
    	   	throw new EntityUnvalidException();
       } else {
    	   	return feed;
       }
    }
    
    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Object> feedbacking(@RequestBody Feedback feed)  throws EntityUnvalidException{
    		long orderNO = feed.getOrderId();
    		if (Long.toString(orderNO) == null) {
    			throw new EntityUnvalidException();
    		}
    		feedbackMap.put(Long.toString(orderNO), feed);
    		return new ResponseEntity<Object>(new StorageResult(true), HttpStatus.OK);
    }
}
