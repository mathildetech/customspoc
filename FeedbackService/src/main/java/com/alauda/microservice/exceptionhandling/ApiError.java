package com.alauda.microservice.exceptionhandling;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

public class ApiError {
	private final String result;
    private final String message;
    private final List<ApiSubError> errors;
    
    public ApiError(String result, String message) {
        this.result = result;
        this.message = message;
        this.errors = new ArrayList<>();
    }
    
    public ApiError(String result, String message, String field) {
        this.result = result;
        this.message = message;
        this.errors = new ArrayList<>();
        this.errors.add(new ApiSubError(field, message));
    }

	public String getResult() {
		return result;
	}

	public String getMessage() {
		return message;
	}
}

class ApiSubError {
	private String field;
	private String message;
	
	public ApiSubError(String field, String message) {
			this.field = field;
			this.message = message;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}
	
}
