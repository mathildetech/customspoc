package com.alauda.microservice.exceptionhandling;

import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(EntityUnvalidException.class)
    protected ResponseEntity<Object> handleEntityNotFound(
    		EntityUnvalidException ex) {
        ApiError apiError = new ApiError("false", ex.getMessage(), ex.getField());
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
