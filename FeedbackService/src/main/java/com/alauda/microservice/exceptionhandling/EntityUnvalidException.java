package com.alauda.microservice.exceptionhandling;

public class EntityUnvalidException extends Exception{
private String field;
  public EntityUnvalidException() {

  }
  
  public EntityUnvalidException(String msg, String field) {
    super(msg);
    this.field = field;
  }
  
  public String getField() {
	  return field;
  }

}
