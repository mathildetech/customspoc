package com.alauda.microservice;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alauda.microservice.bean.Order;
import com.alauda.microservice.bean.OrderLog;

public class PostOrderLog {

	static public void post(String url, Order order, boolean isSuccess, Date start, String reason) {
		String result = "";
		if (isSuccess) {
			result = "成功";
		} else {
			result = "失败";
		}
		String serviceName= "feedback-service";
		
		OrderLog slog = new OrderLog(new Long(order.getOrderId()).toString(), result, reason, serviceName, start, new Date());
		PostRequest.postRequest(url, slog);
	}
}
