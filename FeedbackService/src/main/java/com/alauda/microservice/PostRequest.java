package com.alauda.microservice;

import org.slf4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.alauda.microservice.bean.StorageResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

public class PostRequest {
	private static Logger logger = LoggerFactory.getLogger(PostRequest.class);
	static public boolean postRequest (String url, Object data) {
	RestTemplate restTemplate = new RestTemplate();
	try {
			String jsondata = "";
			jsondata = new ObjectMapper().writeValueAsString(data);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> requestEntity = new HttpEntity<String>(jsondata, headers);

			StorageResult result = restTemplate.postForObject(url, requestEntity, StorageResult.class);
			return true;
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	static public boolean postMS (String url, Object data, int min) {
		RestTemplate restTemplate = new RestTemplate();
		try {
				String jsondata = "";
				jsondata = new ObjectMapper().writeValueAsString(data);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<String> requestEntity = new HttpEntity<String>(jsondata, headers);

				HttpEntity<String> entity = restTemplate.postForEntity(url, requestEntity, String.class);
				String body = entity.getBody();
				logger.error("~~~~~~~~~~~~~~~~~" + body);
				if (body != null || body != "null") {
					return true;
				} else {
					int result = Integer.parseInt(body);
					if (result > min) {
						return true;
					} else {
						return false;
					}
					
				}
			} catch (Exception e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
}
